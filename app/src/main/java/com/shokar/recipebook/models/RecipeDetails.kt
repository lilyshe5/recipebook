package com.shokar.recipebook.models

data class RecipeDetails(val uuid: String? = null,
                         val name: String? = null,
                         val images: MutableList<String>? = null,
                         val lastUpdated: Int? = null,
                         val description: String? = null,
                         val instructions: String? = null,
                         val difficulty: Int? = null,
                         val similar: MutableList<RecipeBrief>? = null)