package com.shokar.recipebook.models

object RecipeApiModel {
    data class RecipesResult(val recipes: MutableList<RecipeList>)

    data class DetailsResult(val recipe: RecipeDetails)
}