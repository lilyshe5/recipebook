package com.shokar.recipebook.models

data class RecipeBrief(val uuid: String? = null,
                       val name: String? = null)