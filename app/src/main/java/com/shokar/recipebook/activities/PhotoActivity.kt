package com.shokar.recipebook.activities

import android.Manifest
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.shokar.recipebook.constants.INTENT
import com.shokar.recipebook.R
import kotlinx.android.synthetic.main.activity_photo.*
import android.graphics.drawable.BitmapDrawable
import android.support.v4.content.ContextCompat
import android.content.pm.PackageManager
import android.widget.Toast
import com.shokar.recipebook.services.RecipeService


class PhotoActivity : AppCompatActivity() {

    private val recipeService = RecipeService()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo)

        val picture = intent.getStringExtra(INTENT.PHOTO)
        val pictureName = intent.getStringExtra(INTENT.PHOTO_NAME)

        Glide.with(this).load(picture).into(recipeImage)

        saveDownloads.setOnClickListener {
            val pictureBitmap = (recipeImage.drawable as BitmapDrawable).bitmap

            val permissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            if (permissionStatus == PackageManager.PERMISSION_GRANTED) {
                 if (recipeService.saveImageToDownloads(pictureBitmap, pictureName))
                     Toast.makeText(this, resources.getString(R.string.saveSuccess), Toast.LENGTH_SHORT).show()
                 else
                     Toast.makeText(this, resources.getString(R.string.error), Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, resources.getString(R.string.permissionDenied), Toast.LENGTH_SHORT).show()
            }
        }

        saveGallery.setOnClickListener{
            val pictureBitmap = (recipeImage.drawable as BitmapDrawable).bitmap

            if (recipeService.saveImageToGallery(pictureBitmap, pictureName, contentResolver))
                Toast.makeText(this, resources.getString(R.string.saveSuccess), Toast.LENGTH_SHORT).show()
            else
                Toast.makeText(this, resources.getString(R.string.error), Toast.LENGTH_SHORT).show()
        }
    }
}