package com.shokar.recipebook.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import com.shokar.recipebook.adapters.RecipeAdapter
import com.shokar.recipebook.services.RecipeService
import com.shokar.recipebook.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import com.shokar.recipebook.models.RecipeList

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private val recipeService = RecipeService()

    private var viewModelJob = Job()
    private val viewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private var adapter: RecipeAdapter? = null
    private var allRecipes: MutableList<RecipeList>? = null

    private val sortOrders = mutableListOf("Name", "Date")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recipeList.layoutManager = LinearLayoutManager(this)

        //initSpinner(searchSpinner, searchOrders)
        initSpinner(sortSpinner, sortOrders)


        this.runOnUiThread {
            progressBar.visibility = View.VISIBLE
        }

        //Checking for network
        if (recipeService.internetIsConnected()) {
            viewModelScope.launch {
                allRecipes = recipeService.loadRecipes()

                adapter = RecipeAdapter(allRecipes!!)
                recipeList.adapter = RecipeAdapter(allRecipes!!)

                this@MainActivity.runOnUiThread {
                    progressBar.visibility = View.GONE
                }
            }
        } else {
            Toast.makeText(this, resources.getString(R.string.noInternetConnection), Toast.LENGTH_SHORT).show()
        }


        //Search handler
        searcher.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val list = this@MainActivity.search(p0.toString())

                if (!list.isNullOrEmpty())
                    recipeList.adapter = RecipeAdapter(list)
            }
        })

    }

    private fun initSpinner(spinner: Spinner, data: MutableList<String>){
        spinner.onItemSelectedListener = this
        spinner.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, data)
    }

    private fun search(text: String): MutableList<RecipeList>?{
        if(!allRecipes.isNullOrEmpty()) {
            return allRecipes!!.filter {
                if (it.description.isNullOrEmpty()) {
                    return@filter it.name!!.toLowerCase().contains(text) ||
                            it.instructions!!.toLowerCase().contains(text)
                } else {
                    return@filter it.name!!.toLowerCase().contains(text) ||
                            it.description.toLowerCase().contains(text) ||
                            it.instructions!!.toLowerCase().contains(text)
                }
            }.toMutableList()
        }
        else
            return null
    }

    private fun sortRecipes(){
        when(sortSpinner!!.selectedItem){
            NAME -> {
                allRecipes!!.sortBy { it.name }
            }
            DATE -> allRecipes!!.sortBy { it.lastUpdated }
        }
        recipeList.adapter = RecipeAdapter(allRecipes!!)
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        if (!allRecipes.isNullOrEmpty())
            sortRecipes()
        val a = (p1 as TextView)
        a.text = " "
    }

    companion object {
        private const val NAME = "Name"
        private const val DATE = "Date"
    }


}
