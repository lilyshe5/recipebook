package com.shokar.recipebook.activities

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import com.shokar.recipebook.constants.INTENT
import com.shokar.recipebook.services.RecipeService
import kotlinx.android.synthetic.main.activity_details.*
import com.shokar.recipebook.R
import android.widget.Toast
import com.shokar.recipebook.adapters.ImageAdapter
import com.shokar.recipebook.adapters.SimilarRecipesAdapter
import com.shokar.recipebook.models.RecipeDetails
import kotlinx.coroutines.*

class DetailsActivity: AppCompatActivity(),
    GestureDetector.OnGestureListener{

    private val recipeService = RecipeService()

    private var mGestureDetector: GestureDetector? = null

    private var recipe = RecipeDetails()

    private var viewModelJob = Job()
    private val viewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private var position = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val uuid = intent.getStringExtra(INTENT.UUID)
        briefList.layoutManager = LinearLayoutManager(this)

        Thread(Runnable {
            this@DetailsActivity.runOnUiThread {
                progressBar.visibility = View.VISIBLE
            }

            try {
                //Checking for network
                if (recipeService.internetIsConnected()) {

                    viewModelScope.launch {
                        recipe = recipeService.loadRecipeDetails(uuid)

                        recipeImage.adapter = ImageAdapter(recipe.images!!)
                        addRecipeInfo()

                        briefList.adapter = SimilarRecipesAdapter(recipe.similar)

                        this@DetailsActivity.runOnUiThread {
                            progressBar.visibility = View.GONE
                        }
                    }

                } else {
                    Toast.makeText(this, resources.getString(R.string.noInternetConnection), Toast.LENGTH_SHORT).show()
                }
            } catch (e: InterruptedException){
                e.printStackTrace()
            }
        }).start()

        mGestureDetector = GestureDetector(this, this)
        recipeImage.setOnTouchListener { _ , motionEvent ->
            mGestureDetector!!.onTouchEvent(motionEvent)
        }

        recipeImage.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(p0: Int) {
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(p0: Int) {
                position = p0
            }
        })
    }

    private fun addRecipeInfo(){
        recipeName.text = recipe.name
        recipeDescription.text = recipe.description
        recipeInstruction.text = recipe.instructions!!.replace("<br>","\n")

        recipeDifficulty.setImageDrawable(resources.getDrawable(resources.getIdentifier
            ("stars${recipe.difficulty}","drawable", packageName)))
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return mGestureDetector!!.onTouchEvent(event)
    }

    override fun onShowPress(p0: MotionEvent?) {
    }

    override fun onSingleTapUp(p0: MotionEvent?): Boolean {
        if (recipeService.internetIsConnected()) {
            val intent = Intent(this, PhotoActivity::class.java)
            intent.putExtra(INTENT.PHOTO, recipe.images!![position])
            intent.putExtra(INTENT.PHOTO_NAME, recipe.name + position.toString())
            startActivity(intent)
        } else {
            Toast.makeText(this, resources.getString(R.string.noInternetConnection), Toast.LENGTH_SHORT).show()
        }
        return false
    }

    override fun onDown(p0: MotionEvent?): Boolean {
        return false
    }

    override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
        return false
    }

    override fun onScroll(p0: MotionEvent?, p1: MotionEvent?, p2: Float, p3: Float): Boolean {
        return false
    }

    override fun onLongPress(p0: MotionEvent?) {

    }

}