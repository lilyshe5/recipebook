package com.shokar.recipebook.api

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.shokar.recipebook.models.RecipeApiModel
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface RecipeApi {

    @GET("recipes")
    fun getRecipes(): Deferred<Response<RecipeApiModel.RecipesResult>>

    @GET("recipes/{uuid}")
    fun getRecipeDetails(@Path("uuid") uuid: String):
            Deferred<Response<RecipeApiModel.DetailsResult>>

    companion object {
        fun create() : RecipeApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(MoshiConverterFactory.create())
                .baseUrl("https://test.kode-t.ru/")
                .build()

            return retrofit.create(RecipeApi::class.java)
        }
    }
}