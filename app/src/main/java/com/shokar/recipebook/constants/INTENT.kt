package com.shokar.recipebook.constants

object INTENT {
    const val UUID = "UUID"
    const val PHOTO = "PHOTO"
    const val PHOTO_NAME = "PHOTONAME"
}