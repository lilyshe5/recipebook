package com.shokar.recipebook.adapters

import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.shokar.recipebook.R
import kotlinx.android.synthetic.main.image_item.view.*

class ImageAdapter(private val data: MutableList<String>): PagerAdapter() {

    override fun isViewFromObject(v: View, p1: Any): Boolean {
        return v === p1 as View
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun instantiateItem(parent: ViewGroup, position: Int): Any {

        val view = parent.inflate(R.layout.image_item, false)

        Glide.with(view.context).load(data[position]).into(view.recipeImage)
        parent.addView(view)

        return view
    }

    override fun destroyItem(parent: ViewGroup, position: Int, `object`: Any) {
        parent.removeView(`object` as View)
    }

}