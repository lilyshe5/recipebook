package com.shokar.recipebook.adapters

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.shokar.recipebook.activities.DetailsActivity
import com.shokar.recipebook.constants.INTENT
import com.shokar.recipebook.R
import com.shokar.recipebook.models.RecipeApiModel
import com.shokar.recipebook.models.RecipeBrief
import kotlinx.android.synthetic.main.brief_item.view.*

class SimilarRecipesAdapter(private val data: MutableList<RecipeBrief>?) :
    RecyclerView.Adapter<SimilarRecipesAdapter.SimilarRecipesHolder>()  {

    class SimilarRecipesHolder(myItem : View) :
        RecyclerView.ViewHolder(myItem),
        View.OnClickListener{

        var view: View = myItem
        var recipe: RecipeBrief? = null

        init {
            view.setOnClickListener(this)
        }

        fun bindItem(recipe: RecipeBrief) {
            this.recipe = recipe

            view.briefName.text = recipe.name
        }


        override fun onClick(p0: View?) {
            val context = itemView.context

            val intent = Intent(view.context, DetailsActivity::class.java)
            intent.putExtra(INTENT.UUID, recipe!!.uuid)

            context.startActivity(intent)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimilarRecipesHolder {
        val inflatedView = parent.inflate(R.layout.brief_item, false)
        return SimilarRecipesHolder(inflatedView)
    }

    override fun getItemCount() = data!!.size

    override fun onBindViewHolder(holder: SimilarRecipesHolder, position: Int) {
        val recipe = data!![position]
        holder.bindItem(recipe)
    }

}