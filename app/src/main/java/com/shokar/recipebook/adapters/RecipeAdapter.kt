package com.shokar.recipebook.adapters

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.shokar.recipebook.activities.DetailsActivity
import com.shokar.recipebook.constants.INTENT
import com.shokar.recipebook.R
import com.shokar.recipebook.models.RecipeList
import kotlinx.android.synthetic.main.recipe_item.view.*

class RecipeAdapter(private val data: MutableList<RecipeList>) :
    RecyclerView.Adapter<RecipeAdapter.RecipeHolder>()  {

    class RecipeHolder(myItem : View) :
        RecyclerView.ViewHolder(myItem),
        View.OnClickListener{

        var view: View = myItem
        var recipe: RecipeList? = null

        init {
            view.setOnClickListener(this)
        }

        fun bindItem(recipe: RecipeList) {
            val context = itemView.context

            this.recipe = recipe

            Glide.with(context).load(recipe.images!![0]).into(view.recipeImage)
            view.recipeName.text = recipe.name
            view.recipeDescription.text = recipe.description
        }


        override fun onClick(p0: View?) {
            val context = itemView.context

            val intent = Intent(view.context, DetailsActivity::class.java)
            intent.putExtra(INTENT.UUID, recipe!!.uuid)

            context.startActivity(intent)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeHolder {
        val inflatedView = parent.inflate(R.layout.recipe_item, false)
        return RecipeHolder(inflatedView)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: RecipeHolder, position: Int) {
        val recipe = data[position]
        holder.bindItem(recipe)
    }

}