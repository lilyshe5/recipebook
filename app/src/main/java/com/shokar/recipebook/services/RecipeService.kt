package com.shokar.recipebook.services

import android.content.ContentResolver
import android.graphics.Bitmap
import android.os.Environment
import android.provider.MediaStore
import com.shokar.recipebook.api.RecipeApi
import com.shokar.recipebook.models.RecipeDetails
import com.shokar.recipebook.models.RecipeList
import java.io.File
import java.io.FileOutputStream

class RecipeService {

    private val recipeService by lazy {
        RecipeApi.create()
    }

    suspend fun loadRecipes(): MutableList<RecipeList>{

        val result = recipeService.getRecipes().await()

        return result.body()!!.recipes
    }

    suspend fun loadRecipeDetails(uuid: String): RecipeDetails{

        val result = recipeService.getRecipeDetails(uuid).await()

        return result.body()!!.recipe
    }

    fun internetIsConnected(): Boolean {
        return try {
            val command = "ping -c 1 google.com"
            Runtime.getRuntime().exec(command).waitFor() == 0
        } catch (e: Exception) {
            false
        }

    }

    fun saveImageToDownloads(picture: Bitmap, name: String): Boolean{
        val foStream: FileOutputStream?
        try {
            val directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            val myPath = File(directory, "$name.jpg")

            foStream = FileOutputStream(myPath)
            picture.compress(Bitmap.CompressFormat.PNG, 100, foStream)
            foStream.close()

            return true
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }

    fun saveImageToGallery(picture: Bitmap, name: String, contentResolver: ContentResolver): Boolean{
        try {
            MediaStore.Images.Media.insertImage(contentResolver, picture, name, name)
            return true
        } catch (e: Exception){
            e.printStackTrace()
        }
        return false
    }

}